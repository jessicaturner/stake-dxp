"""
DxpChain.org StakeMachine
Remote Procedure Calls to DxpChain Node and Bittrex API
DxpChain Management Group Co. Ltd.
"""
# DISABLE SELECT PYLINT TESTS
# pylint: disable=broad-except

# STANDARD PYTHON MODULES
import time
from json import dumps as json_dumps

# PYDXPCHAIN MODULES
from dxpchain.account import Account
from dxpchain.dxpchain import DxpChain
from dxpchain.instance import set_shared_dxpchain_instance
from dxpchain.memo import Memo

# BITTREX MODULES
from bittrex_api import Bittrex
# STAKE DXP MODULES
from config import BROKER, DEV, NODE
from utilities import exception_handler, it, line_info

NINES = 999999999

# CONNECT WALLET TO DXPCHAIN NODE
def pydxpchain_reconnect():
    """
    create locked owner and memo instances of the pydxpchain wallet
    :return: two pydxpchain instances
    """
    pause = 0
    while True:
        try:
            dxpchain = DxpChain(node=NODE, nobroadcast=False)
            set_shared_dxpchain_instance(dxpchain)
            memo = Memo(blockchain_instance=dxpchain)
            return dxpchain, memo
        except Exception as error:
            print(exception_handler(error), line_info())
            time.sleep(0.1 * 2 ** pause)
            if pause < 13:  # oddly works out to about 13 minutes
                pause += 1
            continue


# RPC BLOCK NUMBER
def get_block_num_current():
    """
    connect to node and get the irreversible block number
    :return int(): block number
    """
    dxpchain, _ = pydxpchain_reconnect()
    return dxpchain.rpc.get_dynamic_global_properties()["last_irreversible_block_num"]


# RPC POST WITHDRAWALS
def post_withdrawal_bittrex(amount, client, api, keys):
    """
    send funds using the bittrex api
    :param int(amount): quantity to be withdrawn
    :param str(client): dxpchain username to send to
    :param dict(keys): api keys and secrets for bittrex accounts
    :param int(api): 1, 2, or 3; corporate account to send from
    :return str(msg): withdrawal response from bittrex
    """
    amount = int(amount)
    msg = f"POST WITHDRAWAL BITTREX {amount} {client} {api}, response: "
    print(it("yellow", msg))
    if not DEV:
        try:
            if amount <= 0:
                raise ValueError(f"Invalid Withdrawal Amount {amount}")
            bittrex_api = Bittrex(
                api_key=keys[f"api_{api}_key"], api_secret=keys[f"api_{api}_secret"]
            )
            params = {
                "currencySymbol": "DXP",
                "quantity": str(float(amount)),
                "cryptoAddress": str(client),
            }
            # returns response.json() as dict or list python object
            ret = bittrex_api.post_withdrawal(**params)
            msg += json_dumps(ret)
            if isinstance(ret, dict):
                if "code" in ret:
                    print(it("red", ret), line_info())
                    raise TypeError("Bittrex failed with response code")
        except Exception as error:
            msg += line_info() + " " + exception_handler(error)
            msg += it("red", f"bittrex failed to send {amount} to client {client}",)
            print(msg)
    return msg


def post_withdrawal_pydxpchain(amount, client, memo, keys):
    """
    send DXP with memo to confirm new stake from pydxpchain wallet
    :param int(amount): quantity to be withdrawn
    :param str(client): dxpchain username to send to
    :param dict(keys): contains pydxpchain wallet password for corporate account
    :param str(memo): message to client
    :return str(msg): withdrawal response from pydxpchain wallet
    """
    amount = int(amount)
    msg = f"POST WITHDRAWAL PYDXPCHAIN {amount} {client} {memo}, response: "
    print(it("yellow", msg))
    if not DEV:
        try:
            if amount <= 0:
                raise ValueError(f"Invalid Withdrawal Amount {amount}")
            dxpchain, _ = pydxpchain_reconnect()
            dxpchain.wallet.unlock(keys["password"])
            msg += json_dumps(
                dxpchain.transfer(client, amount, "DXP", memo, account=keys["broker"])
            )  # returns dict
            dxpchain.wallet.lock()
            dxpchain.clear_cache()
        except Exception as error:
            msg += line_info() + " " + exception_handler(error)
            msg += it(
                "red",
                f"pydxpchain failed to send {amount}"
                + f"to client {client} with memo {memo}, ",
            )
            print(msg)
    return msg


# RPC GET BALANCES
def get_balance_bittrex(keys):
    """
    get bittrex DXP balances for all three corporate accounts
    :param keys: dict containing api keys and secrets for 3 accounts
    :return dict(balances):format {1: 0, 2: 0, 3: 0} with int() DXP balance for each api
    """
    balances = {1: NINES, 2: NINES, 3: NINES}
    if not DEV:
        for api in range(1, 4):
            balance = 0
            try:
                bittrex_api = Bittrex(
                    api_key=keys[f"api_{api}_key"], api_secret=keys[f"api_{api}_secret"]
                )
                # returns list() on success or dict() on error
                ret = bittrex_api.get_balances()
                if isinstance(ret, dict):
                    print(it("red", ret), line_info())
                # ret balance will be strigified float; int(float(()) to return integer
                balance = int(
                    float(
                        [i for i in ret if i["currencySymbol"] == "DXP"][0]["available"]
                    )
                )
            except Exception as error:
                print(exception_handler(error), line_info())
            balances[api] = balance
    print("bittrex balances:", balances)
    return balances


def get_balance_pydxpchain():
    """
    get the broker's DXP balance
    :return int(): DXP balance
    """
    try:
        if DEV:
            balance = NINES
        else:
            _, _ = pydxpchain_reconnect()
            account = Account(BROKER)
            balance = int(account.balance("DXP")["amount"])
    except Exception as error:
        balance = 0
        print(exception_handler(error), line_info())
    print("pydxpchain balance:", balance)
    return balance


def authenticate(keys):
    """
    make authenticated request to pydxpchain wallet and bittrex to test login
    :param dict(keys): bittrex api keys and pydxpchain wallet password
    :return bool(): do all secrets and passwords authenticate?
    """
    dxpchain, _ = pydxpchain_reconnect()
    try:
        dxpchain.wallet.unlock(keys["password"])
    except Exception:
        pass
    dxpchain_auth = dxpchain.wallet.unlocked()
    if dxpchain_auth:
        print("PYDXPCHAIN WALLET AUTHENTICATED")
    else:
        print("PYDXPCHAIN WALLET AUTHENTICATION FAILED")
    dxpchain.wallet.lock()
    bittrex_auth = {1: False, 2: False, 3: False}
    try:
        for i in range(3):
            api = i + 1
            bittrex_api = Bittrex(
                api_key=keys[f"api_{api}_key"], api_secret=keys[f"api_{api}_secret"]
            )
            ret = bittrex_api.get_balances()
            if isinstance(ret, list):
                bittrex_auth[api] = True
    except Exception:
        pass
    if all(bittrex_auth.values()):
        print("BITTREX API SECRETS AUTHENTICATED:", bittrex_auth)
    else:
        print("BITTREX API SECRETS FAILED:", bittrex_auth)
    return dxpchain_auth and all(bittrex_auth.values())
